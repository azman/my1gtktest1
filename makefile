# makefile for simple gtk application written in single c source file

ALLAPP = $(subst .c,,$(subst src/,,$(wildcard src/*.c)))
ALLAPP += $(subst .c,,$(wildcard *.c))

GTKVER ?= 3.0

ifeq ($(GTKVER),4)
	GTKPKG=gtk$(GTKVER)
else
	GTKPKG=gtk+-$(GTKVER)
endif


CFLAGS += $(shell pkg-config --cflags $(GTKPKG))
LFLAGS += $(shell pkg-config --libs $(GTKPKG))

.PHONY: dummy $(TARGET)

dummy:
	@echo
	@echo "Run 'make <app>' or 'make <app> GTKVER=2.0'"
	@echo "  <app> = { $(ALLAPP) }"
	@echo

%: %.c
	gcc -Wall $(CFLAGS) -o $@ $+ $(LFLAGS)

clean:
	rm -rf $(ALLAPP)
