/*----------------------------------------------------------------------------*/
#include <gtk/gtk.h>
/*----------------------------------------------------------------------------*/
/* this is somehow undefined when compiling on slackware - older glib? */
#ifndef G_APPLICATION_DEFAULT_FLAGS
#define G_APPLICATION_DEFAULT_FLAGS G_APPLICATION_FLAGS_NONE
#endif
/*----------------------------------------------------------------------------*/
#define MY1APP_FULL "org.my1.gtktest"
#define MY1APP_NAME "GTK Test"
#define TEXT_PX 48
/*----------------------------------------------------------------------------*/
GtkWidget *gwin, *grid, *show;
GtkCssProvider *gcss;
GdkDisplay *disp;
GdkScreen *gscr;
GtkCssProvider *ccss;
/*----------------------------------------------------------------------------*/
void do_insert_style(GtkWidget* widget,char* classname) {
	GtkStyleContext *sctx = gtk_widget_get_style_context(widget);
	if (!gtk_style_context_has_class(sctx,classname))
		gtk_style_context_add_class(sctx,classname);
}
/*----------------------------------------------------------------------------*/
void do_remove_style(GtkWidget* widget,char* classname)
{
	GtkStyleContext *sctx = gtk_widget_get_style_context(widget);
	if (gtk_style_context_has_class(sctx,classname))
		gtk_style_context_remove_class(sctx,classname);
}
/*----------------------------------------------------------------------------*/
gboolean on_show_resize(GtkWidget *widget, GtkAllocation *allocation,
		gpointer user_data) {
	char test[] = ".dynatext { font-size: %dpx; }";
	char buff[128];
	int size = allocation->height*40/50;
	if (size<TEXT_PX) size = TEXT_PX;
	else if (size>3*TEXT_PX) size = 3*TEXT_PX;
	snprintf(buff,128,test,size);
	printf("Resize: %d x %d => %s\n",allocation->width,allocation->height,buff);
	gtk_css_provider_load_from_data(ccss,buff, -1, NULL);
/**
	my1image_view_t* view = (my1image_view_t*) user_data;
	printf("Resize: %d x %d\n",allocation->width,allocation->height);
*/
	return TRUE;
}
/*----------------------------------------------------------------------------*/
static void activate(GtkApplication* app, gpointer data) {
	gwin = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(gwin),MY1APP_NAME);
	grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(gwin),grid);
	show = gtk_label_new("HAHA");
	gtk_widget_set_hexpand(show,TRUE);
	gtk_widget_set_halign(show,GTK_ALIGN_FILL);
	gtk_widget_set_vexpand(show,TRUE);
	gtk_widget_set_valign(show,GTK_ALIGN_FILL);
	gtk_grid_attach(GTK_GRID(grid),show,0,0,1,1);
	gcss = gtk_css_provider_new();
	disp = gdk_display_get_default();
	gscr = gdk_display_get_default_screen(disp);
	gtk_style_context_add_provider_for_screen(gscr,
		GTK_STYLE_PROVIDER(gcss),GTK_STYLE_PROVIDER_PRIORITY_USER);
	gtk_css_provider_load_from_data(gcss,
		".dynatext { font-size: 2em; }\n"
		".bgwhite { background-color: #FFFFFF; }\n"
		".bgtest  { background-color: #4444AA; }", -1, NULL);
	ccss = gtk_css_provider_new();
	gtk_style_context_add_provider_for_screen(gscr,
		GTK_STYLE_PROVIDER(ccss),GTK_STYLE_PROVIDER_PRIORITY_USER);
	do_insert_style(show,"dynatext");
	do_insert_style(show,"bgwhite");
	do_insert_style(grid,"bgtest");
	g_signal_connect(G_OBJECT(show),"size-allocate",
		G_CALLBACK(on_show_resize),0x0);
	/**gtk_window_fullscreen(GTK_WINDOW(gwin));*/
	gtk_widget_show_all(gwin);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char **argv) {
	int stat;
	GtkApplication *gapp;
	gapp = gtk_application_new(MY1APP_FULL,G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect(gapp,"activate",G_CALLBACK(activate),NULL);
	stat = g_application_run(G_APPLICATION(gapp),argc,argv);
	g_object_unref(gapp);
	return stat;
}
/*----------------------------------------------------------------------------*/
